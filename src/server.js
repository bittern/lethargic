/* Offline
 
const { Server, Origins } = require('boardgame.io/server');
const { TicTacToe } = require('./Game');
//const { Chess } = require('./Game'); // second game to play on same server

const server = Server({
  games: [TicTacToe],
//  games: [TicTacToe, Chess], // Include several games to play on the same server
  origins: [Origins.LOCALHOST],
});

server.run(8000);
*/

//*
import { Server, Origins } from 'boardgame.io/server';
import path from 'path';
import serve from 'koa-static';
import { TicTacToe } from './Game';

const server = Server({ games: [TicTacToe],
  origins: [Origins.LOCALHOST],
});
//server.run(8000);
const port = process.env.PORT || 8000;

// Build path relative to the server.js file
const frontEndAppBuildPath = path.resolve(__dirname, '../build');
server.app.use(serve(frontEndAppBuildPath));

server.run(port, () => {
  server.app.use(
    async (ctx, next) => await serve(frontEndAppBuildPath)(
      Object.assign(ctx, { path: '../public/index.html' }),
      next
    )
  );
});


//*/
