// works for local multiplayer
import React from 'react';	
import { Client } from 'boardgame.io/react';
import { TicTacToe } from './Game';	// Game mechanics
import { TicTacToeBoard } from './Board';	// Visual game board
import { SocketIO } from 'boardgame.io/multiplayer';	// enable remote multiplayer


// SERVER IS WORKING REMOTELY, BUT NOT LOCALLY, NEED TO IDENTIFY WHERE THE SERVER IS ON THE LOCAL MACHINE, AND POINT IT THERE. ALSO, CURRENTLY THE ONLY URL REMOTELY IS THE WEBSITE HOSTNMAE, BUT THAT WILL CHANGE WHEN URLS BECOME UNIQUE IN THE GAME
//const serveR = (window.location.hostname === "localhost") ?
//`${window.location.hostname}:8000`:
//`https://${window.location.hostname}`;

//const serveR = `https://${window.location.hostname}`

const serveR = (() => {
  if (window.location.hostname === "localhost") {
    return `localhost:8000`;
  } else {
    return `https://${window.location.hostname}`;
  }
})();

const TicTacToeClient = Client({
  game: TicTacToe,
  board: TicTacToeBoard,
//  multiplayer: SocketIO({ server: 'localhost:8000' }), // local socketIO multiplayer
  multiplayer: SocketIO({ server: serveR }),
});

const App = () => (
  <div>
    <TicTacToeClient playerID="0" />
    <TicTacToeClient playerID="1" />
  </div>
);

export default App;

// Debugging
//window.alert(`hi`);
